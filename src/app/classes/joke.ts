export class Joke {
    public $key: string;
    public text: string;
    public author: string;
    public answer: string;
    public score = 0;

    public reset(){
        this.text = '';
        this.author = 'Anonymous';
        this.answer = null;
        this.score = 0;
    }
}
