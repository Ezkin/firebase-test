import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { BackendService } from './services/backend.service';

import { AngularFireModule } from 'angularfire2';
import { JokeBookComponent } from './joke-book/joke-book.component';

// Must export the config
export const firebaseConfig = {
    apiKey: 'AIzaSyBQamjkBLYy55JiWk7dQRMevMByywfO_w4',
    authDomain: 'test-angular2-a1e36.firebaseapp.com',
    databaseURL: 'https://test-angular2-a1e36.firebaseio.com',
    projectId: 'test-angular2-a1e36',
    storageBucket: 'test-angular2-a1e36.appspot.com',
    messagingSenderId: '1059996147503'
};

@NgModule({
  declarations: [
    AppComponent,
    JokeBookComponent,
    JokeBookComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [BackendService],
  bootstrap: [AppComponent]
})
export class AppModule { }
