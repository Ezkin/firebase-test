import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JokeBookComponent } from './joke-book.component';

describe('JokeBookComponent', () => {
  let component: JokeBookComponent;
  let fixture: ComponentFixture<JokeBookComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JokeBookComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JokeBookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
