import { Component, OnInit } from '@angular/core';
import { FirebaseListObservable } from 'angularfire2';

import { BackendService } from '../services/backend.service';

import { Joke } from '../classes/joke';

@Component({
  selector: 'app-joke-book',
  templateUrl: './joke-book.component.html',
  styleUrls: ['./joke-book.component.scss']
})
export class JokeBookComponent implements OnInit {

  newJoke = new Joke();
  jokeList: FirebaseListObservable<any>;
  showJokeForm = false;

  constructor(private _backendService: BackendService) {
  }

  ngOnInit() {
    this.newJoke.reset();
    this.jokeList = this._backendService.getJokes();
  }

  add(){
    if(this.newJoke.text.length <= 0){
      return;
    }
    this._backendService.add(this.newJoke);
    this.newJoke.reset();
    this.showJokeForm = false;
  }

  downvote(joke: Joke){
    this._backendService.downvote(joke.$key, joke.score - 1);
  }

  upvote(joke: Joke){
    this._backendService.upvote(joke.$key, joke.score + 1);
  }
}
