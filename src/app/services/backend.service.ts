import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import { Observable } from "rxjs/Rx";
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { Joke } from '../classes/joke';

import {AngularFire, AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable} from 'angularfire2';

@Injectable()
export class BackendService {

  private _backendDatabase: FirebaseListObservable<Joke[]>;

  constructor(private http: Http, af: AngularFire) {
    this._backendDatabase = af.database.list('/jokes');
  }

  public getJokes(){
    return this._backendDatabase;
  }

  public add(joke: Joke) {
    this._backendDatabase.push(joke);
  }

  public upvote(key: string, score: number) {
      this._backendDatabase.update(key, { score : score});
  }

  public downvote(key: string, score: number) {
    if(score < -10){
      this._backendDatabase.remove(key);
    }else{
      this._backendDatabase.update(key, { score : score});
    }
  }

  public remove(jokeId: string){

  }
}
