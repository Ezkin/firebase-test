import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { JokeBookComponent } from './joke-book/joke-book.component';

const routes: Routes = [
  {
    path: '',
    children: [],
    component: JokeBookComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
