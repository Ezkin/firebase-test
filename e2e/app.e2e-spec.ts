import { FunckyFoodPage } from './app.po';

describe('funcky-food App', () => {
  let page: FunckyFoodPage;

  beforeEach(() => {
    page = new FunckyFoodPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
