#Create Project

    ng new funcky-food --routing true --style scss

#Add components

    ng g component 

#Add services

    ng g service

#Add classes 

    ng g class 

#Run 

    ng serve